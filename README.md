# Modeling dispersion in porous media #

### What is this repository for? ###

This code is for modeling macro-dispersion in porous media with Markov models.

* Markov models in time for simulation dispersion in random porous networks
* Stencil method and Extended stencil method

### How do I get set up? ###

* Dependencies: Python2.7, Matplotlib, Numpy, Scipy, pyamg